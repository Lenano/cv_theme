<?php 
  if (function_exists('get_header')) {
    get_header();
  } else {
    die();
  }
?>

<div class="presentation-container">
	<div class="presentation-text">
		<h1> </h1>
		
	</div>
</div> <!-- .presentation-container -->

<div class="header-halfmoon-two">
	<img src="<?php echo get_bloginfo('template_url') ?>/img/bula-white.png" alt="Decoration half moon white"/>
</div>



<!-- The Loop -->
<div class="post-container">
	<h1 class="title-center"> Arbetserfarenhet och utbildning </h1>
	<div class="flex-container">
	<?php

	if( have_posts() ){
		while( have_posts() ){
			the_post(); ?>

	<article class="post <?php if(has_post_thumbnail()) { ?> has-thumbnail <?php }?>"> 
			<!-- post-thumbnail -->
		<div class='post-thumbnail'>
			<?php the_post_thumbnail( 'small-thumbnail'); ?>
		</div>

		<h3><a href=<?php the_permalink()?>> <?php the_title(); ?></a></h3> 

		<!-- to get the category -->
		<?php
		$categories = get_the_category();
		$separator = ", ";
		$output = "";

		if ($categories) {
			foreach ($categories as $category) {
				$output .= '<a href="' . get_category_link($category) . '">' . $category->cat_name . '</a>' . $separator; 
			}
			?>
			<p> <?php echo trim($output, $separator); ?> </p>
		<?php
		} 
		?>

		<p>
			<!-- The excerpt -->
			<?php> 
			$excerpt = get_the_excerpt(); 
			echo $excerpt;
			?>
			<a href="<?php the_permalink()?>"> Läs mer &raquo;  </a>
		</p>
	</article>
		
		<?php
		}
	}else{
	echo '<p> Inga inlägg hittades </p>';
	} ?>
	</div>
</div> <!-- .post-container -->



<div class="header-halfmoon-three">
	<img src="<?php echo get_bloginfo('template_url') ?>/img/bula-eggshell.png" alt="Decoration half moon eggshell"/>
</div>


<!-- Above footer widget area -->
<div class="above-footer-container">

		<?php if( is_active_sidebar( 'sidebar2' )) { ?>
		<div class="above-footer-widget">
			<?php  dynamic_sidebar( 'sidebar2' );  ?>
		</div>
	<?php } ?>

		<?php if( is_active_sidebar( 'sidebar3' )) { ?>
		<div class="above-footer-widget">
			<?php  dynamic_sidebar( 'sidebar3' );  ?>
		</div>
	<?php } ?>

</div>
</div> <!-- .container  header.php -->

<!-- Footer -->

<?php get_footer(); ?> 





