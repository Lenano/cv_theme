
	<?php wp_footer(); ?> <!-- wp needs this to add the necessary code for plugins, widgets etc -->
	

	<footer class='site-footer'>

		<div class="text-search">
			<?php if( is_active_sidebar( 'sidebar1' )) { ?>
			<div class="footer-text-widget">
				<?php  dynamic_sidebar( 'sidebar1' );  ?>
			</div>
				<?php } ?>
	
			<div class="search-form">
				<?php get_search_form();?>
			</div>
		</div> <!-- text-search -->

		<div class="footer-name-year">
				<!-- Name and year -->
			<p> <?php bloginfo( 'name' )?> - &copy; <?php echo date( 'Y' ); ?></p>
		</div>
	</footer>
	
</body>
</html>