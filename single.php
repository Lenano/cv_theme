<?php 
// single page
get_header(); 
?>

<div class="post-container-single">
	<?php
	// The loop
	if( have_posts() ){
		while( have_posts() ){
			the_post(); ?>

		<article class="post"> 

			<div class="thumbnail-large">
				<?php the_post_thumbnail( 'large-thumbnail'); ?>
			</div>

			<div class="post-container-content">
				<h2><?php the_title(); ?></h2> 
		
				<?php> the_content() ?>
			</div>


		</article>
		
		<?php
		}
	}else{
	echo '<p> Inga inlägg hittades </p>';
	} ?>
</div> <!-- .post-container -->



<?php
// Footer
get_footer(); 

?> 



