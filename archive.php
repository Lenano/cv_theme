<?php 
// Archive
get_header(); 
?>

<!-- The Loop -->
<div class="post-container">
	
	<?php

	if( have_posts() ){ ?>

		<h1 class="category-title"><?php 

		if ( is_category() ) {
		echo "Kategori: ";
		echo single_cat_title();
		} elseif ( is_tag() ) {
			single_tag_title();
		} elseif ( is_author() ) {
			the_post();
		}else {
			echo "Tyvärr hittades inte det du sökte.";
		} ?> 
		</h1> <?php


		while( have_posts() ){
			the_post(); ?>

	<article class="post <?php if(has_post_thumbnail()) { ?> has-thumbnail <?php }?>"> 
			<!-- post-thumbnail -->
		<div class='post-thumbnail'>
			<?php the_post_thumbnail( 'small-thumbnail'); ?>
		</div>

		<h3><a href=<?php the_permalink()?>> <?php the_title(); ?></a></h3> 

		<!-- to get the category -->
		<?php
		$categories = get_the_category();
		$separator = ", ";
		$output = "";

		if ($categories) {
			foreach ($categories as $category) {
				$output .= '<a href="' . get_category_link($category) . '">' . $category->cat_name . '</a>' . $separator; 
			}
			//$output = strtoupper($output); 
			?>
			<p> <?php echo trim($output, $separator); ?> </p>
			<?php
		}
		
		 ?>

		<p>
			<!-- The excerpt -->
			<?php> 
			$excerpt = get_the_excerpt(); 
			echo $excerpt;
			?>
			<a href="<?php the_permalink()?>"> Läs mer &raquo;  </a>
		</p>
	</article>
		
		<?php
		}
	}else{
	echo '<p> Inga inlägg hittades </p>';
	} ?>

</div> <!-- .post-container -->


<!-- Footer -->
<?php get_footer(); ?> 





