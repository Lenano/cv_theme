<!DOCTYPE html>
<html>

<head <?php language_attributes();?>>
	<meta charset="<?php bloginfo( 'charset' ); ?>">   <!-- charaters-->
	<meta name="viewport" content="width=device-width">  <!-- responsive-->
	<meta name="format-detection" content="telephone=no" />
	<title><?php bloginfo('name'); ?></title> <!-- not optimal for search engines! Change later -->

	<?php wp_head(); ?> <!-- wp needs this to add necessary code for plugins etc -->

</head>

<body <?php body_class(); ?>> <!-- to target different pages with css-->

	<div class="container">
		<!-- site-header-->
		<div class="<?php if(is_front_page()) { 
						echo 'header-wrapper';} else {echo 'site-nav-other';}?>">

			<header class='site-header'>

				<div class='header-title'>
					<?php

					$frontpage_id = get_option( 'page_on_front' );

					$field = get_field( 'stor_rubrik');


					if(is_front_page()) {
						while(have_posts()) {
							the_post();
							?>
							<h1><?php echo $field; ?></h1>
							<?php the_field('mindre_text', $frontpage_id); ?> 
							<?php
						}
					}
					?>
				
			
					<nav class=" <?php if(is_front_page()) { 
						echo 'site-nav';} else {echo 'site-nav-other';}?> ">
						<?php 
						$args = array(
							'theme_location' => 'primary'
						);
						?>
				
						<?php wp_nav_menu( $args ); ?>
					</nav>
	

				</div> <!-- header-title -->

			</header> <!-- site-header-->
		</div>    <!--header-wrapper-->

		<?php
		
		if ( is_front_page() ) { ?>
		<div class="header-halfmoon-eggshell">
			<img class="eggshell" src="<?php echo get_bloginfo('template_url') ?>/img/bula-eggshell.png" alt="Decoration half moon shape"/>
		</div>
		<?php } ?>
	