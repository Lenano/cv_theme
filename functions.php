 <?php 
// functions.php

add_action('wp_enqueue_scripts', 'add_theme_scripts'); // en hook
function add_theme_scripts() {
	wp_enqueue_style('main', get_template_directory_uri() . '/style.css', null, '1.0', 'all');
}

add_action('after_setup_theme', 'my_theme_setup');
// Theme setup
function my_theme_setup() {
	// Navigations Menues
	register_nav_menus(array(
		'primary'=>__( 'Primary Menu' ),
		'footer'=>__( 'Footer Menu' ),
		));
	// Add featured image support
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'small-thumbnail', 50, 70, true );
	add_image_size( 'large-thumbnail', 120, 145, true );
}

// Get custom excerpt lenght ( index.php )
function custom_excerpt_length() {
	return 3;
}

add_filter( 'excerpt_length', 'custom_excerpt_length'); // Hook that replaces the built in excerpt length (55 words) to a custom length (3 words)



// Add Widget Location
function myWidgetInit() {

	register_sidebar( array (
		'name' => 'Footer Sidebar 1',
		'id' => 'sidebar1'
		));

	register_sidebar( array (
	'name' => 'Above Footer Sidebar 2',
	'id' => 'sidebar2'
	));

	register_sidebar( array (
	'name' => 'Above Footer Sidebar 3',
	'id' => 'sidebar3'
	));
}

add_action('widgets_init', 'myWidgetInit');


















































