<?php 
// Page
get_header(); 
?>

<div class="intro-text">
	<?php
	$url = home_url();
	if( have_posts() ){
		while( have_posts() ){
			the_post(); ?>

		<article class="post"> 
			<h1><?php the_title(); ?></h1> 
			<p><?php> the_content() ?></p>
		</article>
		
		<?php
		}
	}else{
	echo '<p> Ingen sida hittades. Tillbaka till <a href="<?php echo esc_url( $url ); ?>">startsidan</a> </p>';
	} ?>
</div> <!-- .post-container -->


<?php
// Footer
get_footer(); 

?> 



