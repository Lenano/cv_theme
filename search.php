<?php 
get_header()
?>
	

				


<!-- The Loop -->
<div class="post-container-search">

	<div class="result">
		<h1> Du sökte efter "<?php echo get_search_query(); ?>" </h1> 
		<h3> <?php echo $wp_query->found_posts . ' ' ?> resultat 	hittades:   </h3>
	</div>
	
	<?php

	if( have_posts() ){
	?><div class="searchresult-boxes"><?php
		while( have_posts() ){
			the_post(); 

	?>

		<div class="searchbox"> 

			<div class="image">
				<div class="title">
					<h3><a href=<?php the_permalink()?>> <?php the_title(); ?></a></h3>
				</div>
			</div> 

			<div class="meta">
				<div class="categories">

					<?php $cats = get_the_category(); 
				
					foreach($cats as $cat) {
					echo $cat->name;
					}
					?>	
				</div> <!-- categories -->

				<div class="date">
				<?php echo get_the_date() ?>
					
				</div>  <!-- date -->
			</div> <!-- meta -->


		</div> <!-- .searchbox -->
		
		<?php
		}
	}else{
	?> <p> Inga resultat hittades </p> <?php
	} ?>
		
	</div> <!-- .searchresult-boxes -->	
</div> <!-- .post-container-search -->

<?php
get_footer()
?>