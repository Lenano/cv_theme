<?php 

    get_header();
 ?>

<!-- The Loop -->
<div class="post-container category-post">
	<h1 class="category-title"> <?php 
	if ( is_category() ) {
		echo "Kategori: ";
		echo single_cat_title();
		} ?> </h1>
	<div class="flex-container">
	<?php

	if( have_posts() ){
		while( have_posts() ){
			the_post(); ?>

	<article class="post <?php if(has_post_thumbnail()) { ?> has-thumbnail <?php }?>"> 
			<!-- post-thumbnail -->
		<div class='post-thumbnail'>
			<?php the_post_thumbnail( 'small-thumbnail'); ?>
		</div>

		<h3><a href=<?php the_permalink()?>> <?php the_title(); ?></a></h3> 

		<!-- to get the category -->
		<?php
		$categories = get_the_category();
		$separator = ", ";
		$output = "";

		if ($categories) {
			foreach ($categories as $category) {
				$output .= '<a href="' . get_category_link($category) . '">' . $category->cat_name . '</a>' . $separator; 
			}
			?>
			<p> <?php echo trim($output, $separator); ?> </p>
		<?php
		} 
		?>

		<p>
			<!-- The excerpt -->
			<?php> 
			$excerpt = get_the_excerpt(); 
			echo $excerpt;
			?>
			<a href="<?php the_permalink()?>"> Läs mer &raquo;  </a>
		</p>
	</article>
		
		<?php
		}
	}else{
	echo '<p> Inga inlägg hittades </p>';
	} ?>
	</div>
</div> <!-- .post-container -->


</div> <!-- .container  header.php -->

<!-- Footer -->

<?php get_footer(); ?> 





