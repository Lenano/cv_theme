<?php 
/*
* Template Name: Min portfolio
*/
get_header(); 
?>

<div class="intro-text">
	<?php

	if( have_posts() ){
		while( have_posts() ){
			the_post(); ?>

		<article class="post"> 
			
			<h1> <?php the_title(); ?> </h1> 
		
			<?php> the_content() ?>
		</article>
		
		<?php
		}
	}else{
	echo '<p> Inga inlägg hittades </p>';
	} ?>

</div> 


<?php
// To display portfolio 
$args = array( 
    'post_type' => 'portfolio',  
);

$portfolio = new WP_Query( $args ); ?>

<aside  class="clear">
<?php
while ( $portfolio->have_posts() ) { 
	$portfolio->the_post(); ?>
    <div class="item">
     	<h3 class="entry-title"> <?php the_title()?> </h3>
     	<div class="entry-content">
     	<?php the_content(); ?>
     	</div>
    </div>
<?php 
}
?>
</aside>
<?php
wp_reset_query();
?>


<?php
// Footer
get_footer(); 

?> 